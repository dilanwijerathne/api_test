'use strict'

const Helpers = use('Helpers')
const Ride = use('App/Models/Ride');
const { validate } = use('Validator');
const randomString = require('random-string')




class HomeController {



// request for a new ride if user is logged in
	async requestRide({request,response,auth,error}){
		await auth.check()

		
		// retrive cookie for further actions 
		const cookie = request.cookie('rideRequester')

		 const rules = {
	      destination:'required',
	      date:'required',
	      time:'required'
    	};

    	const validation = await validate(request.all(), rules);

    	 if (validation.fails()) {
	      session
	        .withErrors(validation.messages())
	        .flashExcept(['destination']);

	      return response.redirect('back')
	    }


		try{
			  const ride = new  Ride
	          ride.destination(input('destination'))
	          ride.ref(randomString({length : 40}))
	          ride.destination(input('date'))
	          ride.destination(input('time'))
	          ride.save()



		      session.flash({
		      notification:{
		        type: 'success',
		        message:'Product save successful'
		      }
		    });

		}catch(error){
			response.send(error.message)
		}



      response.header('Content-type', 'application/json',200)
      response.cookie('rideRequester', 20)
      response.send(ride)


	}


	// check ride status 
	async checkRideStatus({request,response,auth,error}){
		await auth.check()

		// retrive cookie for further actions 
		const cookie = request.cookie('rideRequester')


		const rules = {
	      ride_ref:'required'
	   
    	};

    	const validation = await validate(request.all(), rules);

    	 if (validation.fails()) {
	      session
	        .withErrors(validation.messages())
	        .flashExcept(['ride_ref']);

	      return response.redirect('back')
	    }



		try{
			 const ride = await ride.query()
			.where('ref',ride_ref)
			.fetch()



		      session.flash({
		      notification:{
		        type: 'success',
		        message:'Product save successful'
		      }
		    });

		}catch(error){
			response.send(error.message)
		}



		response.header('Content-type', 'application/json',200)
      	response.cookie('rideRequester', 20)
      	response.send(ride)

	}

}

module.exports = HomeController
