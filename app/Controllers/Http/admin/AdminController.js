'use strict'

class AdminController {

  index({view}){
     return view.render('admin/dashboard')
  }

}

module.exports = AdminController
