'use strict'

const { validateAll } = use('Validator')
const User = use('App/Models/User')
const Category = use('App/Models/Category')
const CategoryDao = use('App/Models/CategoryDao')

const randomString = require('random-string')
const Mail = use('Mail')
class RegisterController {

	async register({request,session,response}){
		// validate frominputs
		const validation = await validateAll(request.all(),{
			fname:'required',
			lname:'required',
			address:'required',
			email:'required|unique:users,email',
			phone:'required',
			RegPassword:'required'

		})

		if(validation.fails()){
			session.withErrors(validation.messages()).flashExcept(['RegPassword'])

			return response.redirect('back')
		}
		// create 

		
		const user = await User.create({
			f_name: request.input('fname'),
			l_name: request.input('lname'),
			address: request.input('address'),
			email: request.input('email'),
			phone: request.input('phone'),
			password: request.input('RegPassword'),
			confirmationToken: randomString({length : 40})
		})

		// send confirmation email
		
		await Mail.send('emails.confirm_email',user.toJSON(),message=>{
			message.to(user.email)
			.from('dilanbw@sample.com')
			.subject('Please test user reg')

		})

		
		// display success message
		session.flash({
			notification:{
				type: 'success',
				message:'Registration successful ! an mail has been sent to your email address, please confirm'
			}
		})

		return response.redirect('back')

	}




	show({view}){
		return view.render('login')
	}


	  itemCetegories(){
	
		const x  = new CategoryDao
		const k = x.getCategories()		
		return k
	}











}

module.exports = RegisterController
