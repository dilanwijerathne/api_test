'use strict'

const { validateAll } = use('Validator')
const User = use('App/Models/User')
const Hash = use('Hash')

class LoginController {

	async login({request,auth,session,response}){

		//get from data
		const {loginUsername, loginPassword, remember} = request.all();
		//retrive user
		const user = await User.query()
		.where('email',loginUsername)
		.where('u_status',1)
		.first()


		//verify username and password 
		if(user){
			const passwordVerified = await Hash.verify(loginPassword,user.password)
			if(passwordVerified){

				//login to user
				await auth.remember(!!remember).login(user)

				return response.route('/')
			}
		}

		// if user now found, display an error message 
		session.flash({
			notification : {
				type: 'danger',
				message: 'We are unable to find such profile with given credentials'
			}
		})

		return response.redirect('back')

	}
}

module.exports = LoginController
