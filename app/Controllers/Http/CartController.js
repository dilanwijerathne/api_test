'use strict'

class CartController {


  cartCheckout({view}){
    return view.render('cart')
  }

}

module.exports = CartController
