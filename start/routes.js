'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use('Route')



// regiter a new user 
Route.post('/api/v1/register_user', 'Auth/RegisterController.register').as('registerUser')


// login a user 
Route.get('/api/v1//login','Auth/HomeController.login')
Route.post('/api/v1//login','Auth/LoginController.login').as('login')

// user logout
Route.get('/api/v1/logout', async function({auth}){
    return await auth.logout()
});

// request for a new ride
Route.get('/api/v1//request_ride:/destination?/date?/time?','HomeController.request_ride')

// check the status of a ride
Route.get('/api/v1//check_ride_status:/ride_ref?','HomeController.checkRideStatus')

