'use strict'

const Schema = use('Schema')

class ProductSchema extends Schema {
  up () {
    this.create('products', (table) => {
      table.increments()
      table.string('unique_id',150).notNullable()
      table.string('item_name',150).notNullable()
      table.integer('item_category',11).notNullable()
      table.text('item_description','longtext').notNullable()
      table.integer('item_seller',11).notNullable()
      table.string('item_seller_note',300)
      table.string('item_manufacture',150)
      table.string('item_serial',150)
      table.string('item_location',150)
      table.double('item_price',12,5)
      table.double('item_discount',12,5)
      table.integer('item_stock',150)
      table.string('item_delivery_method',150)
      table.double('item_delivery_charges',12,5)
      table.double('item_installation_charges',12,5)
      table.double('item_tax',12,5)
      table.double('item_other_charges',12,5)
      table.integer('item_stock_status',11)
      table.integer('item_sort_order',11)
      table.integer('status',11).notNullable()
      table.string('meta_title',150)
      table.string('meta_description',170)
      table.string('meta_keywords',450)
      table.integer('views',11)
      table.timestamps()
    })
  }

  down () {
    this.drop('products')
  }
}

module.exports = ProductSchema
