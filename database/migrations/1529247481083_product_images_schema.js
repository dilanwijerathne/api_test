'use strict'

const Schema = use('Schema')

class ProductImagesSchema extends Schema {
  up () {
    this.create('product_images', (table) => {
      table.increments()
      table.string('image_name')
      table.string('image_location')
      table.string('image_uploaded_by')
      table.string('image_product_id')
      table.timestamps()
    })
  }

  down () {
    this.drop('product_images')
  }
}

module.exports = ProductImagesSchema
