'use strict'

const Schema = use('Schema')

class ProductTagsSchema extends Schema {
  up () {
    this.create('product_tags', (table) => {
      table.increments(),
      table.string('product_tag_name'),
      table.string('product_tag_createdBy'),
      table.timestamps()
    })
  }

  down () {
    this.drop('product_tags')
  }
}

module.exports = ProductTagsSchema
