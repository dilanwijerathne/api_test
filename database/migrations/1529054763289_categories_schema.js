'use strict'

const Schema = use('Schema')

class CategoriesSchema extends Schema {
  up () {
    this.create('categories', (table) => {
      table.increments()
      table.string('category_name',50).notNullable()
      table.string('category_link',250).notNullable()
      table.integer('parent_category', 11).notNullable()
      table.integer('is_main', 2)
      table.integer('status', 2)
      table.timestamps()
    })
  }

  down () {
    this.drop('categories')
  }
}

module.exports = CategoriesSchema
