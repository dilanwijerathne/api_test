'use strict'

const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('username', 80).notNullable().unique()
      table.string('email', 254).notNullable().unique()
      table.string('password', 60)
      table.string('f_name', 254)
      table.string('l_name', 254)
      table.string('address', 254)
      table.string('phone', 20)
      table.integer('u_role', 2).defaultTo(1)
      table.integer('u_status', 2).defaultTo(0)
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
